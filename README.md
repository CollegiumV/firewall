firewall
========
Configures host firewall, managing access by applying services to zones (following the firewalld model)

Supported Operating Systems
---------------------------
* CentOS 7
* Void Linux

Requirements
------------
No Requirements

Role Variables
--------------
None

Role Defaults
-------------
```
---
# RedHat
firewalld_minimalmark: <int> (minimal MARK value that will be used in iptables) 100
firewalld_cleanuponexit: <string> (if firewall rules should be removed on stop) 'yes'
firewalld_lockdown: <string> (if firewalld changes are only accepted from whitelisted programs) 'no'
firewalld_ipv6_rpfilter: <string> (reverse path filter for IPv6) 'no'
firewalld_individualcalls: <string> (if individual calls are used to startup firewalld) 'no'

# Void
iptables_input_policy: <string> (default iptables input policy) 'DROP'
iptables_logging_limit: <string> (limit for logged packets) '2/min'
iptables_logging_prefix: <string> (prefix for logged packets) 'iptables: '
iptables_logging_jump: <string> (logging jump traget) 'RETURN'

# General
firewall_enabled: <string> (if firewall rules should be applied or debugged) true
firewall_log_dropped: <string> (if dropped packets should be logged) 'no'
firewall_default_zone: <string> (default firewall zone) 'public'
firewall_base_services: <list> services to be applied immediately
  - name: <string> (the name of the service) ssh
    description: <string> (a short description of the service) 'Secure shell remote administration'
    zones: <list> zones to apply the services to
      - '{{ firewall_default_zone }}'
    rules: <list> rules the service needs
      - protocol: <string> tcp
        port: <int> 22

firewall_zones: <list> firewall zones to be created
  - zone: <string> '{{ firewall_default_zone }}'
    description: <string> 'Default Firewall Zone'
```

Dependencies
------------
* site-base

Setting Firewall rules from other roles
---------------------------------------
The `add-service.yml` tasks has been provided to apply firewall rules from other roles. The format for doing so is the following:

Include the tasks from the firewall role in the desired role:

```
---
- name: Add firewall rules
  include_role:
    name: firewall
    tasks_from: add-service.yml
  vars:
    firewall_services: '{{ firewall_rolename_services }}'
```

Define the firewall rules to be added as firewall_rolename_services in the desired role in the `vars` directory:

```
---
firewall_rolename_services:
  - name: rolename
    description: 'Good description'
    zones: '{{ rolename_firewall_zones }}'
    rules:
      - protocol: udp
        port: 123
```

`rolename_firewall_zones` should default to a list containing only the default firewall zone.

```
---
rolename_firewall_zones:
  - '{{ firewall_default_zone }}'
```

This can be further customized by setting the values of these lists to variables that the users can override, i.e. setting the port number to `rolename_port`, defaulting that value to 22 and allowing the user to override it.

`firewall_reload_now` can additionally be set to true to apply the rules immediately, instead of waiting for the handler.

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning
* ssh access after provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
